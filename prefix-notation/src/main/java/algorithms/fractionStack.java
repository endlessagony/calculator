// Copyright

package algorithms;

import java.util.ArrayList;

public class fractionStack extends studentStack{
    private ArrayList<Fraction> myFractionStack;

    public fractionStack(){
        myFractionStack = new ArrayList<>();
    }

    public void printFractionStack(){
        for (int i = 0; i < myFractionStack.size(); i++){
            myFractionStack.get(i).getFraction();
        }
        System.out.println(" ");
    }

    public ArrayList<Fraction> pushInFractionStack(int numerator, int denominator) {
        Fraction usersFraction = new Fraction(numerator, denominator);
        myFractionStack.add(usersFraction);
        return myFractionStack;
    }

    public ArrayList<Fraction> pushInFractionStack(Fraction fraction){
        myFractionStack.add(fraction);
        return myFractionStack;
    }

    public Fraction popOfFractionStack(){
        Fraction poppedElement = myFractionStack.get(myFractionStack.size() - 1);
        myFractionStack.remove(myFractionStack.size() - 1);
        return poppedElement;
    }

    public boolean isEmpty(){
        boolean condition = false;
        if (myFractionStack.size() == 0){
            condition = true;
        }
        return condition;
    }

    public Fraction peekOfMyFractionStack(){
        return myFractionStack.get(myFractionStack.size() - 1).getFraction();
    }

}
