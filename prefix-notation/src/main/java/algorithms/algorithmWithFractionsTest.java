// Copyright

package algorithms;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class algorithmWithFractionsTest {//тесты на класс подчсёта обратной польской записи
    //и перевод строки в обратную польскую

    @Test
    void calculationsWithFractionsAttemptNumberOne() {
        algorithmWithFractions checkTheAnswer = new algorithmWithFractions("1/2 5/2 : 2/1 +");
        assertEqualsForFractions(new Fraction(11,5), checkTheAnswer.calculationsWithFractions());

    }

    @Test
    void calculationsWithFractionsAttemptNumberTwo() {
        algorithmWithFractions checkTheAnswer = new algorithmWithFractions("1/2 9/6 * 2/4 -");
        assertEqualsForFractions(new Fraction(1,4), checkTheAnswer.calculationsWithFractions());

    }

    @Test
    void calculationsWithFractionsAttemptNumberThree() {
        algorithmWithFractions checkTheAnswer = new algorithmWithFractions("1/2 5/2 : 2/1 + bv ere");
        assertEquals(null, checkTheAnswer.calculationsWithFractions());

    }

    @Test
    void calculationsWithFractionsAttemptNumberFour() {
        algorithmWithFractions checkTheAnswer = new algorithmWithFractions("1/2 *");
        assertEquals(null, checkTheAnswer.calculationsWithFractions());

    }

    @Test
    void theOriginalAttemptNumberOne() {
        algorithmWithFractions checkTheAnswer = new algorithmWithFractions("1/2 : 5/2 + 2/1");
        String answer = checkTheAnswer.theOriginal();
        assertEquals(" 1/2 5/2: 2/1+", answer);
    }

    @Test
    void theOriginalAttemptNumberTwo() {
        algorithmWithFractions checkTheAnswer = new algorithmWithFractions("1/2 : 5/2 + 2/1 - 5/1 : 1/5");
        String answer = checkTheAnswer.theOriginal();
        assertEquals(" 1/2 5/2: 2/1+ 5/1 1/5:-", answer);
    }

    public boolean assertEqualsForFractions(Fraction fractionExpected, Fraction fractionActual){
        boolean answer = false;
        if (fractionExpected.getDenominator() == fractionActual.getDenominator()
                && fractionExpected.getNominator() == fractionActual.getNominator()){
            answer = true;
        }
        return answer;
    }//метод для проверки
    //правильности вывода с классом дробей

}