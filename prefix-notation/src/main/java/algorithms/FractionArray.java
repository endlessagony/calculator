// Copyright

package algorithms;
import java.util.ArrayList;

public class FractionArray {
    private ArrayList<Fraction> fractionArray = new ArrayList();

    public FractionArray(String string){
        String[] splitString= string.split(" ");
        for (int i = 0; i < splitString.length; i++){
            Fraction fraction = new Fraction(splitString[i]);
            fractionArray.add(i, fraction);
        }
    }// Создание массива дробей путем передачи строки вида "m/n p/q ..."

    public FractionArray(int num, int bottom, int top){
        for (int i = 0; i < num; i++){
            Fraction fraction = new Fraction();
            fraction.randomFraction(bottom, top);
            fractionArray.add(i, fraction);
        }
    } // Создание массива случайных дробей

    public Fraction arraySum(){
        if (fractionArray.size() > 1){
            for (int i =1; i < fractionArray.size(); i++) {
                fractionArray.get(0).sum(fractionArray.get(i));
            }
        }
        return fractionArray.get(0);
    } //Седующие методы производят операции над дробями: сложение, вычитание, умножение, деление. Причем результат сохраняется в первой дроби массива

    public Fraction arraySubtract(){
        if (fractionArray.size() > 1){
            for (int i =1; i < fractionArray.size(); i++) {
                fractionArray.get(0).subtract(fractionArray.get(i));
            }
        }
        return fractionArray.get(0);
    }

    public Fraction arrayMultiply(){
        if (fractionArray.size() > 1){
            for (int i =1; i < fractionArray.size(); i++) {
                fractionArray.get(0).multiply(fractionArray.get(i));
            }
        }
        return fractionArray.get(0);
    }

    public Fraction arrayDivide(){
        if (fractionArray.size() > 1){
            for (int i =1; i < fractionArray.size(); i++) {
                fractionArray.get(0).divide(fractionArray.get(i));
            }
        }
        return fractionArray.get(0);
    }

    public void arrayPower(int power){
        for (int i =0; i < fractionArray.size(); i++){
            fractionArray.get(i).power(power);
        }
    } // Возведение всей дробей массива в степень

    public Fraction getMax(){
        Fraction max = fractionArray.get(0);
        for (int i = 0; i < fractionArray.size(); i++){
            if (fractionArray.get(i).isGreater(max)) {
                max = fractionArray.get(i);
            }
        }
        return max;
    }

    public Fraction getMin(){ // Возвращает минимальный элемент массива
        Fraction min = fractionArray.get(0);
        for (int i = 0; i < fractionArray.size(); i++){
            if (!(fractionArray.get(i).isGreater(min)))
                min = fractionArray.get(i);
        }
        return min;
    }

    public boolean maxMinusOne(){// Возвращает максимальный -1 элемент массива
        boolean exists = false;
        Fraction one = new Fraction(1,1);
        Fraction maxMinusOne = getMax().subtract(one);
        System.out.println("Индексы элементов равных макс -1:");
        for (int i = 0; i < fractionArray.size(); i++){
            if ((fractionArray.get(i).getNominator() == maxMinusOne.getNominator()) && (fractionArray.get(i).getDenominator()== maxMinusOne.getDenominator())){
                System.out.print(i + " ");
                exists = true;
            }
        }
        System.out.println("");
        return exists;
    }

    public boolean minPlusOne(){  // Возвращает минимальный +1 элемент массива
        boolean exists = false;
        Fraction one = new Fraction(1,1);
        Fraction minPlusOne = getMin().sum(one);
        System.out.println("Индексы элементов равных мин + 1:");
        for (int i = 0; i < fractionArray.size(); i++){
            if ((fractionArray.get(i).getNominator() == minPlusOne.getNominator()) && (fractionArray.get(i).getDenominator()== minPlusOne.getDenominator())){
                System.out.print(i + " ");
                exists = true;
            }
        }
        System.out.println("");
        return exists;
    }

    public Fraction getPreviousToMax(){ // Возврат предыдущего к максимальному
        Fraction previousToMax = getMin();
        for (int i = 0; i < fractionArray.size(); i++){
            if (fractionArray.get(i).isGreater(previousToMax) && !(fractionArray.get(i).isGreater(getMax()))){
                previousToMax = fractionArray.get(i);
            }
        }
        return previousToMax;
    }

    public  Fraction getNextToMin(){// Возврат следующего к минимальному
        Fraction nextToMin = getMax();
        for (int i = 0; i < fractionArray.size(); i++){
            if (fractionArray.get(i).isLess(nextToMin) && !(fractionArray.get(i).isLess(getMin()))){
                nextToMin = fractionArray.get(i);
            }
        }
        return nextToMin;
    }


    public void outFractionArrayAfterOperation(){ //Вывод первого элемента массива, используется после операций сложения, вычитания...
        System.out.println();
        fractionArray.get(0).getFraction();
    }

    public void outFractionArray(){// Вывод всех дробей массива
        System.out.println();
        for (int i = 0; i < fractionArray.size(); i++){
            fractionArray.get(i).getFraction();
        }
    }

    public String toString(){
        String myString = "";
        for (int i = 0 ; i < fractionArray.size(); i++){
            myString = myString + " " + fractionArray.get(i).toString();
        }
        return myString;
    }


}
