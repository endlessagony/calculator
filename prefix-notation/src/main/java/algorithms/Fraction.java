// Copyright

package algorithms;

import java.util.Random;


public class Fraction {
    private int nominator;
    private int denominator;

    public Fraction(){}

    public Fraction(int nominator, int denominator){
        this.nominator = nominator;
        this.denominator = denominator;
    } //Создание дроби путем передачи числителя и знаменателя

    public void randomFraction(int bottom, int top){// Создание дроби случайным образом, задав границы выбора случайного элемента
        Random random = new Random();
        if (bottom <= top) {
            nominator = random.nextInt(top - bottom) + bottom;
            denominator = random.nextInt(top - bottom) + bottom;
        }
        else{
            System.out.println("Некорректное задание параметров случайного массива");
        }
        new Fraction(nominator, denominator);
    }

    public Fraction (String fraction){ //Создание дроби путем передачи выражения вида "m/n"
        String[] fractionArray = fraction.split("/");
        nominator = Integer.parseInt(fractionArray[0]);
        denominator = Integer.parseInt(fractionArray[1]);
    }

    public void setDenominator(int denominator) {
        this.denominator = denominator;
    }

    public void setNominator(int nominator) {
        this.nominator = nominator;
    }

    public int getNominator() {
        return nominator;
    }

    public int getDenominator() {
        return denominator;
    }

    public boolean isGreater(Fraction myFraction){
        Fraction newFraction= new Fraction(nominator, denominator);
        newFraction.setDenominator(denominator * myFraction.denominator);
        newFraction.setNominator(nominator * myFraction.denominator - denominator * myFraction.nominator);
        return newFraction.nominator >= 0;
    } //Методы сравнения

    public boolean isLess(Fraction myFraction){
        Fraction newFraction= new Fraction(nominator, denominator);
        newFraction.setDenominator(denominator * myFraction.denominator);
        newFraction.setNominator(nominator * myFraction.denominator - denominator * myFraction.nominator);
        return newFraction.nominator <= 0;
    }

    public Fraction sum( Fraction f2){
        int nom = nominator * f2.denominator + f2.nominator * denominator;
        int denom = denominator * f2.denominator;
        return new Fraction(nom, denom);
    }// следующие методы представляют из себя операции над дроби: сложение, вычитание, умножение, деление, возведение в любую степень

    public Fraction subtract(Fraction f2){
        int nom = nominator * f2.denominator - f2.nominator * denominator;
        int denom = denominator * f2.denominator;
        return new Fraction(nom, denom);
    }

    public Fraction multiply(Fraction f2){
        int nom = nominator * f2.nominator;
        int denom = denominator * f2.denominator;
        nominator = nom;
        denominator = denom;
        Fraction result = new Fraction(nom, denom);
        return result;
    }

    public Fraction divide(Fraction f2){
        int nom = nominator * f2.denominator;
        int denom = denominator * f2.nominator;
        nominator = nom;
        denominator = denom;
        Fraction result = new Fraction(nom, denom);
        return result;
    }

    public void power( int power){
        int nom = nominator;
        int denom = denominator;
        if ( power > 0){
            for (int i = 1; i < power; i++){
                nom = nom * nominator;
                denom = denom * denominator;
            }
            nominator = nom;
            denominator = denom;
        }
        else if (power < 0){
            for (int i = power; i < -1; i++){
                nom = nom * nominator;
                denom = denom * denominator;
            }
            nominator = denom;
            denominator = nom;
        }
        else {
            nominator = 1;
            denominator = 1;
        }
    }

    public Fraction getFraction(){
        if (nominator * denominator < 0){
            nominator = -1 * Math.abs(nominator);
            denominator = Math.abs(denominator);
        }
        else if (nominator * denominator > 0){
            nominator = Math.abs(nominator);
            denominator = Math.abs(denominator);
        }
        else if(nominator == 0){
            denominator = 1;
        }
        else {
            System.out.println("Деление на ноль невозможно");
        }
        if (nominator >= denominator){
            for (int i = nominator; i > 0; i--){
                if (nominator % i == 0 && denominator % i == 0){
                    nominator = nominator/i;
                    denominator = denominator/i;
                }
            }
        }
        else{
            for (int i = denominator; i > 0; i--) {
                if (nominator % i == 0 && denominator % i == 0) {
                    nominator = nominator / i;
                    denominator = denominator / i;
                }
            }
        }

        System.out.print(nominator + "/" + denominator + " ");
        return new Fraction(nominator,denominator);
    } //Метод вывода дроби в консоль, сокращает дробь и устраняет непорядки со знаками

    public String toString(){
        if (nominator * denominator < 0){
            nominator = -1 * Math.abs(nominator);
            denominator = Math.abs(denominator);
        }
        else if (nominator * denominator > 0){
            nominator = Math.abs(nominator);
            denominator = Math.abs(denominator);
        }
        else if(nominator == 0){
            denominator = 1;
        }
        else {
            System.out.println("Деление на ноль невозможно");
        }
        if (nominator >= denominator){
            for (int i = nominator; i > 0; i--){
                if (nominator % i == 0 && denominator % i == 0){
                    nominator = nominator/i;
                    denominator = denominator/i;
                }
            }
        }
        else{
            for (int i = denominator; i > 0; i--) {
                if (nominator % i == 0 && denominator % i == 0) {
                    nominator = nominator / i;
                    denominator = denominator / i;
                }
            }
        }
        return (nominator+"/"+denominator);
    }
}
