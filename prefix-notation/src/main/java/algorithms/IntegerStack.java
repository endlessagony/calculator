// Copyright

package algorithms;

import org.w3c.dom.ls.LSOutput;

import java.lang.constant.Constable;
import java.util.ArrayList;

public class IntegerStack {
    private ArrayList<Integer> myStack;

    public IntegerStack(){
        myStack = new ArrayList<>();
    }

    public String toString(){
        String myString = myStack.toString();
        return myString;
    }

    public ArrayList<Integer> push(int usersInput){
        myStack.add(usersInput);
        return myStack;
    }

    public int pop(){
        int poppedElement = myStack.get(myStack.size() - 1);
        myStack.remove(myStack.size() - 1);
        return poppedElement;
    }

    public int sizeOfTheStack() {
        int size = 0;
        for (int i = 0; i < myStack.size(); i++){
            if (myStack.get(i) != null){
                size++;
            }
        }
        return size;
    }

    public boolean isEmpty(){
        boolean condition = false;
        if (sizeOfTheStack() == 0){
            condition = true;
        }
        return condition;
    }

    public int peek(){
        if (myStack.isEmpty()){
            return 0;
        }
        else {
            return myStack.get(myStack.size() - 1);
        }
    }
}
