// Copyright

package algorithms;

import java.util.ArrayList;

public class studentStack {
    private ArrayList<String> myStack;

    public studentStack(){
        myStack = new ArrayList<>();
    }

    public String toString(){
        String myString = myStack.toString();
        return myString;
    }

    public ArrayList<String> pushIn(String usersString){
        myStack.add(usersString);
        return myStack;
    }

    public String popOf(){
        String poppedElement = myStack.get(myStack.size() - 1);
        myStack.remove(myStack.size() - 1);
        return poppedElement;
    }

    public int sizeOfTheStack() {
        int size = 0;
        for (int i = 0; i < myStack.size(); i++){
            if (myStack.get(i) != null){
                size++;
            }
        }
        return size;
    }

    public String peekOf(){
        if (myStack.isEmpty()){
            return "";
        }
        else {
            return myStack.get(myStack.size() - 1);
        }
    }
}
