// Copyright

package algorithms;

import java.util.*;

public class algorithm {
    private String usersString;//строка пользователя
    private IntegerStack myStack = new IntegerStack();//стэк целочисленный
    private ArrayList<String> usersArrayList;//массив элементов строки
    private String outputResult = "";//выходная строка для обратной польской записи
    private studentStack myOwnStackOfSigns = new studentStack();//стэк знаков (стэк состоящий из элементов типа данных String)


    public algorithm(String myInput){
        this.usersString = myInput;
    }//конструктор моего алгоритма

    public algorithm() {
    }//данный мето был создам автоматически компилятором,
    // из-за того, что класс алгоритма дробей наследует некоторые функции из класса алгоритм
    // (такие как проверка на операнда, приоритет операций и т.д.)

    private ArrayList initStringArray(){
        usersArrayList = new ArrayList<>(Arrays.asList(usersString.split("")));
        return usersArrayList;
    }//инициализация массива элементов входной строки

    public boolean condition(String element){
        boolean conditionForElements = true;
        if (element.equals("*") |
                (element.equals("+")) |
                (element.equals(":")) |
                (element.equals("-"))){
            conditionForElements = false;
        }
        return conditionForElements;
    }//условие для различие элемента от операнда и операции

    public Integer /*void*/ calculations(){
        if (CheckCorrectInput()) {
            //usersArrayList = new ArrayList<>(Arrays.asList(usersString.split("")));
            initStringArray();
            for (int i = 0; i < usersString.length(); i++) {
                if (condition(usersArrayList.get(i))) {
                    myStack.push(Integer.parseInt(usersArrayList.get(i)));
                } else {
                    int firstTemporary = 0;
                    int secondTemporary = 0;
                    try {
                        firstTemporary = myStack.pop();
                        secondTemporary = myStack.pop();
                    }
                    catch (Exception E){
                        return null;
                    }
                    //int firstTemporary = myStack.pop();
                    //int secondTemporary = myStack.pop();
                    if (usersArrayList.get(i).equals("*")) {
                        myStack.push(firstTemporary * secondTemporary);
                    } else if (usersArrayList.get(i).equals("+")) {
                        myStack.push(firstTemporary + secondTemporary);
                    } else if (usersArrayList.get(i).equals("-")) {
                        myStack.push(secondTemporary - firstTemporary);
                    } else if (usersArrayList.get(i).equals(":")) {
                        myStack.push(secondTemporary / firstTemporary);
                    }
                }
            }
            if (myStack.isEmpty()) {
                System.out.println("Некорректный ввод. Пожалуйста, повторите попытку.");
            }
            return myStack.peek();
        }
        else{
            System.out.println("Неккоректный ввод. Пожалуйста, повторите попытку.");
        }
        //return myStack.peek();
        if (myStack.isEmpty()){
            //throw new Exception("");
            return null;
        }
        else{
            return myStack.peek();
        }
    }//калькулятор обратной польской записи

    public String theOriginal(){
        initStringArray();
        for (int i = 0; i < usersArrayList.size(); i++) {
            if (condition(usersArrayList.get(i))) {
                outputResult = outputResult + usersArrayList.get(i);
            }
            else {
                if (myOwnStackOfSigns.sizeOfTheStack() == 0){
                    myOwnStackOfSigns.pushIn(usersArrayList.get(i));
                }
                else if (myOwnStackOfSigns.sizeOfTheStack() >= 1) {
                    if (priority(myOwnStackOfSigns.peekOf()) >= priority(usersArrayList.get(i))){
                        while (priority(myOwnStackOfSigns.peekOf()) >= priority(usersArrayList.get(i))) {
                            outputResult = outputResult + myOwnStackOfSigns.popOf();
                            //my2OwnStack.pushIn(usersArrayList.get(i));
                            if ((priority(myOwnStackOfSigns.peekOf()) < priority(usersArrayList.get(i))) || myOwnStackOfSigns.sizeOfTheStack() == 0){
                                myOwnStackOfSigns.pushIn(usersArrayList.get(i));
                                break;
                            }
                        }
                    }
                    else if (priority(myOwnStackOfSigns.peekOf()) < priority(usersArrayList.get(i))){
                        myOwnStackOfSigns.pushIn(usersArrayList.get(i));
                    }
                }
            }
        }
        if (myOwnStackOfSigns.sizeOfTheStack() > 0) {
            String operations = "";
            for (int j = 0; j <= myOwnStackOfSigns.sizeOfTheStack(); j++) {
                String temporary = myOwnStackOfSigns.popOf();
                operations = operations + temporary;
            }
            outputResult = outputResult + operations;
        }

        return outputResult;
    }//перевод входной строки в обратную польскую запись

    public int priority(String element){
        int priority = 0;
        if (element.equals("*") | element.equals(":")) {
            priority += 3;
        }
        else if (element.equals("+") | element.equals("-")) {
            priority += 2;
        }
        else if (element.equals("")){
            priority = 0;
        }
        return priority;
    }//метод определения приоритета операций

    public boolean CheckCorrectInput(){
        boolean check = true;
        String correctInput = usersString.replaceAll("[^\\d /+\\-*:]", "");
        if (usersString.length() != correctInput.length()) {
            check = false;
        }
        return check;
    }//метод проверки на правильность входной строки
}
