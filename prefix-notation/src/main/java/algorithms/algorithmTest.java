// Copyright

package algorithms;
import static org.junit.jupiter.api.Assertions.*;

class algorithmTest {//тесты на подсчёт обратной польской записи
    //и перевода строки в обратную польскую запись

    @org.junit.jupiter.api.Test
    void calculationsAttemptNumberOne() {
        algorithm firstTest = new algorithm("25*63:+");
        assertEquals(12,firstTest.calculations());
    }

    @org.junit.jupiter.api.Test
    void calculationsAttemptNumberTwo() {
        algorithm firstTest = new algorithm("93:69*-");
        assertEquals(-51,firstTest.calculations());
    }

    @org.junit.jupiter.api.Test
    void calculationsAttemptNumberThree() {
        algorithm firstTest = new algorithm("93:69*-ndfsdfre");
        assertEquals(null, firstTest.calculations());
    }

    @org.junit.jupiter.api.Test
    void calculationsAttemptNumberFour() {
        algorithm firstTest = new algorithm("93:69*-+");
        assertEquals(null, firstTest.calculations());
    }

    @org.junit.jupiter.api.Test
    void theOriginalStringAttemptNumberOne() {
        algorithm firstTestWithOriginalString = new algorithm("2*3+6");
        assertEquals("23*6+",firstTestWithOriginalString.theOriginal());
    }

    @org.junit.jupiter.api.Test
    void theOriginalStringAttemptNumberTwo() {
        algorithm firstTestWithOriginalString = new algorithm("2+9*3-5*3+3");
        assertEquals("293*+53*-3+",firstTestWithOriginalString.theOriginal());
    }

    @org.junit.jupiter.api.Test
    void theOriginalStringAttemptNumberThree() {
        algorithm firstTestWithOriginalString = new algorithm("2+9*3-5*3+3");
        assertEquals("293*+53*-3+",firstTestWithOriginalString.theOriginal());
    }

    @org.junit.jupiter.api.Test
    void FTest(){
        String initialString = "2+3*5+2";
        algorithm attemptWithOriginalStringWithIntegersExpression = new algorithm(initialString);
        String prefixNotation = attemptWithOriginalStringWithIntegersExpression.theOriginal();
        assertEquals("235*+2+", prefixNotation);
        algorithm newAlgorithm = new algorithm(prefixNotation);
        int result = newAlgorithm.calculations();
        assertEquals(19, result);
    }

}