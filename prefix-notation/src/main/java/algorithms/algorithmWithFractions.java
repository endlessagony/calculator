// Copyright

package algorithms;

import java.util.ArrayList;
import java.util.Arrays;

public class algorithmWithFractions extends algorithm{
    private String usersStringOfFractions;//задаваемая строка пользователя
    private ArrayList<String> stringArrayOfUsersInput;//массив элментов строки
    private fractionStack stackOfFractions = new fractionStack();//стэк дробей
    private String outputResultForFractions = "";//выхожная трока для вывода обратной польской записи
    private studentStack stackOfSigns = new studentStack();//стэк знако для вывода обратной польской записи

    public algorithmWithFractions(String myInput) {
        usersStringOfFractions = myInput;
    }//конструктор моего алгоритма

    private ArrayList initializationOfArray(){
        stringArrayOfUsersInput = new ArrayList(Arrays.asList(usersStringOfFractions.split(" ")));
        return stringArrayOfUsersInput;
    }//инициализация массива элементов строки

    public Fraction calculationsWithFractions(){
        if (CheckCorrectInput()) {
            initializationOfArray();
            for (int i = 0; i < stringArrayOfUsersInput.size(); i++) {
                if (condition(stringArrayOfUsersInput.get(i))) {
                    stackOfFractions.pushInFractionStack(new Fraction(stringArrayOfUsersInput.get(i)));
                } else {
                    Fraction firstTemporary = new Fraction();
                    Fraction secondTemporary = new Fraction();
                    try {
                        firstTemporary  = stackOfFractions.popOfFractionStack();
                        secondTemporary = stackOfFractions.popOfFractionStack();
                    }
                    catch (Exception E){
                        return null;
                    }
                    if (stringArrayOfUsersInput.get(i).equals("*")) {
                        stackOfFractions.pushInFractionStack(firstTemporary.multiply(secondTemporary));
                    } else if (stringArrayOfUsersInput.get(i).equals("+")) {
                        stackOfFractions.pushInFractionStack(firstTemporary.sum(secondTemporary));
                    } else if (stringArrayOfUsersInput.get(i).equals("-")) {
                        stackOfFractions.pushInFractionStack(secondTemporary.subtract(firstTemporary));
                    } else if (stringArrayOfUsersInput.get(i).equals(":")) {
                        stackOfFractions.pushInFractionStack(secondTemporary.divide(firstTemporary));
                    }
                }
            }
            if (stackOfFractions.isEmpty()) {
                System.out.println("Некорректный ввод. Пожалуйста, повторите попытку.");
            }
            return stackOfFractions.peekOfMyFractionStack();
        }
        else{
            System.out.println("Неккоректный ввод. Пожалуйста, повторите попытку.");
        }
        if (stackOfFractions.isEmpty()){
            return null;
        }
        else {
            return stackOfFractions.peekOfMyFractionStack().getFraction();
        }
    }//калькулятор обратной польской записи

    @Override
    public String theOriginal(){
        initializationOfArray();
        for (int i = 0; i < stringArrayOfUsersInput.size(); i++) {
            if (condition(stringArrayOfUsersInput.get(i))) {
                if (i == 0) {
                    outputResultForFractions = outputResultForFractions + stringArrayOfUsersInput.get(i);//данное условие сделано лишь для красоты вывода строки
                }
                else {
                    outputResultForFractions = outputResultForFractions + " " + stringArrayOfUsersInput.get(i);
                }
            }
            else {
                if (stackOfSigns.sizeOfTheStack() == 0){
                    stackOfSigns.pushIn(stringArrayOfUsersInput.get(i));
                }
                else if (stackOfSigns.sizeOfTheStack() >= 1) {
                    if (priority(stackOfSigns.peekOf()) >= priority(stringArrayOfUsersInput.get(i))){
                        while (priority(stackOfSigns.peekOf()) >= priority(stringArrayOfUsersInput.get(i))) {
                            outputResultForFractions = outputResultForFractions + " " + stackOfSigns.popOf();
                            if ((priority(stackOfSigns.peekOf()) < priority(stringArrayOfUsersInput.get(i))) || stackOfSigns.sizeOfTheStack() == 0){
                                stackOfSigns.pushIn(stringArrayOfUsersInput.get(i));
                                break;
                            }
                        }
                    }
                    else if (priority(stackOfSigns.peekOf()) < priority(stringArrayOfUsersInput.get(i))){
                        stackOfSigns.pushIn(stringArrayOfUsersInput.get(i));
                    }
                }
            }
        }
        if (stackOfSigns.sizeOfTheStack() > 0) {
            String operations = "";
            for (int j = 0; j <= stackOfSigns.sizeOfTheStack(); j++) {
                String temporary = stackOfSigns.popOf();
                operations = operations + " " + temporary;
            }
            outputResultForFractions = outputResultForFractions + operations;
        }

        return outputResultForFractions;
    }//перевод строки в обратную польскую запись

    @Override
    public boolean CheckCorrectInput(){
        boolean check = true;
        String correctInput = usersStringOfFractions.replaceAll("[^\\d /+\\-*:]", "");
        if (usersStringOfFractions.length() != correctInput.length()){
            check = false;
        }
        return check;
    }//проврека входной строки на правильность
}
