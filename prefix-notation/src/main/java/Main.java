// Copyright

import algorithms.*;

public class Main {
    public static void main(String[] args) {
        algorithm lesson = new algorithm("2+");
        lesson.calculations();
        if (lesson.calculations() == null) {
            System.out.println("Неккоректный ввод. Пожалуйста, повторите попытку.");
        }//проверка на корректность строки, если строка некорректна - вывод сообщения об ошибке
        else {
            System.out.println(lesson.calculations());
        }
        algorithm attemptWithOriginalStringWithIntegersExpression = new algorithm("2+3*5+2");
        System.out.println("Ваша обратная польская запись: " + attemptWithOriginalStringWithIntegersExpression.theOriginal());
        algorithmWithFractions lessonAttemptTwoWithFractions = new algorithmWithFractions("1/2 -");
        lessonAttemptTwoWithFractions.calculationsWithFractions();
        if (lessonAttemptTwoWithFractions.calculationsWithFractions() == null) {
            System.out.println("Неккоректный ввод. Пожалуйста, повторите попытку.");
        }//проверка на корректность строки, если строка некорректна - вывод сообщения об ошибке
        else {
            System.out.println(lessonAttemptTwoWithFractions.calculations());
        }
        algorithmWithFractions FractionExpressionToPolska = new algorithmWithFractions("1/2 + 1/6 * 6/2 - 2/2");
        System.out.println("Ваша обратная польская запись: " + FractionExpressionToPolska.theOriginal());

    }
}