// Copyright

import algorithms.*;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

class JUnitAndFunctionalTests {
    @org.junit.jupiter.api.Test
    void JUnitcalculationsAttemptNumberOne() {
        algorithm firstTest = new algorithm("25*63:+");
        assertEquals(12, firstTest.calculations());
    }

    @org.junit.jupiter.api.Test
    void JUnitcalculationsAttemptNumberTwo() {
        algorithm firstTest = new algorithm("93:69*-");
        assertEquals(-51, firstTest.calculations());
    }

    @org.junit.jupiter.api.Test
    void JUnitcalculationsAttemptNumberThree() {
        algorithm firstTest = new algorithm("93:69*-ndfsdfre");
        assertEquals(null, firstTest.calculations());
    }

    @org.junit.jupiter.api.Test
    void JUnitcalculationsAttemptNumberFour() {
        algorithm firstTest = new algorithm("93:69*-+");
        assertEquals(null, firstTest.calculations());
    }

    @org.junit.jupiter.api.Test
    void JUnittheOriginalStringAttemptNumberOne() {
        algorithm firstTestWithOriginalString = new algorithm("2*3+6");
        assertEquals("23*6+", firstTestWithOriginalString.theOriginal());
    }

    @org.junit.jupiter.api.Test
    void JUnittheOriginalStringAttemptNumberTwo() {
        algorithm firstTestWithOriginalString = new algorithm("2+9*3-5*3+3");
        assertEquals("293*+53*-3+", firstTestWithOriginalString.theOriginal());
    }

    @org.junit.jupiter.api.Test
    void JUnittheOriginalStringAttemptNumberThree() {
        algorithm firstTestWithOriginalString = new algorithm("2+9*3-5*3+3");
        assertEquals("293*+53*-3+", firstTestWithOriginalString.theOriginal());
    }

    @Test
    void JUnitcalculationsWithFractionsAttemptNumberOne() {
        algorithmWithFractions checkTheAnswer = new algorithmWithFractions("1/2 5/2 : 2/1 +");
        assertEqualsForFractions(new Fraction(11,5), checkTheAnswer.calculationsWithFractions());

    }

    @Test
    void JUnitcalculationsWithFractionsAttemptNumberTwo() {
        algorithmWithFractions checkTheAnswer = new algorithmWithFractions("1/2 9/6 * 2/4 -");
        assertEqualsForFractions(new Fraction(1,4), checkTheAnswer.calculationsWithFractions());

    }

    @Test
    void JUnitcalculationsWithFractionsAttemptNumberThree() {
        algorithmWithFractions checkTheAnswer = new algorithmWithFractions("1/2 5/2 : 2/1 + bv ere");
        assertEquals(null, checkTheAnswer.calculationsWithFractions());

    }

    @Test
    void JUnitcalculationsWithFractionsAttemptNumberFour() {
        algorithmWithFractions checkTheAnswer = new algorithmWithFractions("1/2 *");
        assertEquals(null, checkTheAnswer.calculationsWithFractions());

    }

    @Test
    void JUnittheOriginalAttemptNumberOne() {
        algorithmWithFractions checkTheAnswer = new algorithmWithFractions("1/2 : 5/2 + 2/1");
        String answer = checkTheAnswer.theOriginal();
        assertEquals("1/2 5/2 : 2/1 +", answer);
    }

    @Test
    void JUnittheOriginalAttemptNumberTwo() {
        algorithmWithFractions checkTheAnswer = new algorithmWithFractions("1/2 : 5/2 + 2/1 - 5/1 : 1/5");
        String answer = checkTheAnswer.theOriginal();
        assertEquals("1/2 5/2 : 2/1 + 5/1 1/5 : -", answer);
    }

    @org.junit.jupiter.api.Test
    void FTestOne(){
        String initialString = "2+3*5+2";
        algorithm attemptWithOriginalStringWithIntegersExpression = new algorithm(initialString);
        String prefixNotation = attemptWithOriginalStringWithIntegersExpression.theOriginal();
        assertEquals("235*+2+", prefixNotation);
        algorithm newAlgorithm = new algorithm(prefixNotation);
        int result = newAlgorithm.calculations();
        assertEquals(19, result);
    }

    @org.junit.jupiter.api.Test
    void FTestTwo(){
        String initialString = "2+3*9+2";
        algorithm attemptWithOriginalStringWithIntegersExpression = new algorithm(initialString);
        String prefixNotation = attemptWithOriginalStringWithIntegersExpression.theOriginal();
        assertEquals("239*+2+", prefixNotation);
        algorithm newAlgorithm = new algorithm(prefixNotation);
        int result = newAlgorithm.calculations();
        assertEquals(31, result);
    }

    @org.junit.jupiter.api.Test
    void FTestThree(){
        String initialString = "1/2 : 5/2 + 2/1 - 5/1 : 1/5";
        algorithmWithFractions attemptWithOriginalStringWithIntegersExpression = new algorithmWithFractions(initialString);
        String prefixNotation = attemptWithOriginalStringWithIntegersExpression.theOriginal();
        assertEquals("1/2 5/2 : 2/1 + 5/1 1/5 : -", prefixNotation);

        algorithmWithFractions calculations = new algorithmWithFractions(prefixNotation);
        Fraction result = calculations.calculationsWithFractions();
        assertEqualsForFractions(new Fraction(-114, 5), result);
    }

    public boolean assertEqualsForFractions(Fraction fractionExpected, Fraction fractionActual){
        boolean answer = false;
        if (fractionExpected.getDenominator() == fractionActual.getDenominator()
                && fractionExpected.getNominator() == fractionActual.getNominator()){
            answer = true;
        }
        return answer;
    }
}