import os
import sys

if __name__ == '__main__':
    path = "prefix-notation/"
    if not os.path.exists(path):
        print(f"Path {path} does not exist")
        sys.exit(1)

    for root, dirs, files in os.walk(path):
        for file in files:
            if file.endswith(".java"): 
                file_path = os.path.join(root, file)
                with open(file_path, "r") as f:
                    first_line = f.readline().strip()
                    if first_line != "// Copyright":
                        print(f"Copyright not found in {file_path}")
                        sys.exit(1)

    sys.exit(0)
